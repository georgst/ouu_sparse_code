function G_Hessian = J_Hess(z, invF, U2, Dinv, u0, SnuEr_t, nu_old, beta)
% return the Hessian of G applied to the direction z

    function x_result = ApplySnur(x)
    %function z_result = ApplySnur(z, n, invF, U2, Dinv)
        x_result = Dinv*x - (Dinv*(U2*(invF*(U2'*(Dinv*x)))));
    end


numerator = u0.*ApplySnur(u0.*z);
for i = 1:size(SnuEr_t, 2)
    numerator = numerator + SnuEr_t(:,i).*(ApplySnur(SnuEr_t(:,i).*z));
end

G_Hessian = -2*(beta*numerator - (nu_old.^(-3)).*z);

end
