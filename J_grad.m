function [u0, SnuEr_t, G_gradient, re_nu] = J_grad(n, invF, U2, Dinv, w_0, Er_t, epsilon, nu_old)
% return value of G and reweighting update

    function x_result = ApplySnur(x)
    %function z_result = ApplySnur(z, n, invF, U2, Dinv)
        x_result = Dinv*x - (Dinv*(U2*(invF*(U2'*(Dinv*x)))));
    end

u0 = ApplySnur(w_0); % S_nu_r * w_0
SnuEr_t = ApplySnur(Er_t); % matrix E

tmp = zeros(n*n,1);
for i = 1:size(Er_t,2)
    tmp = tmp + SnuEr_t(:,i).^2;
end
G_gradient = u0.^2 + tmp + epsilon^2 - nu_old.^(-2);
re_nu = 1./sqrt(u0.^2 + tmp + epsilon^2);

end




