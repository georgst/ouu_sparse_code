MATLAB code to reproduce examples presented in

Sparse solutions in optimal control of PDEs with uncertain parameters:
the linear case, 2018. [arXiv:1804.05678]

by Chen Li and Georg Stadler

Simply run the main file; the parameters and settings are found on the
top of this m-file:

> ouu

