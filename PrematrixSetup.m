function res = PrematrixSetup(nu_old, invF, beta, U2, Dinv, u0, SnuEr_t)
% return the digonal preconditioner

W = invF * U2';

tmp = diag(Dinv) - (diag(Dinv).^2).*sum(U2.*W', 2);

tmp_new = (u0.^2).*tmp;
for i = 1:size(SnuEr_t, 2)
    tmp_new = tmp_new + (SnuEr_t(:,i).^2).*tmp;
end

res = 1./(-2*beta*tmp_new + 2*nu_old.^(-3));
