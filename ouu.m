% Soution of optimal control governed by SPDEs with jointly sparse
% controls. The codes solves
%
% min_u   1/2 int_Omega int_D (y - yd)^2 + alpha*u^2 dx dmu + beta |u|_L12,
%
% where |u|_L12 = int_D (int_Omega u^2 dmu)^1/2 dx
%
% is a sparsity term enforcing shared sparsity.
%
% The problem is formulated using iterative norm-reweighting (IRLS)
% and a Newton-CG variant. The physical domain D is the 2D unit square
% and the governing PDE is either the Laplace or the Helmhotz equation.
%
% Details in "Sparse solutions in optimal control of PDEs with uncertain
%             parameters: the linear case", arXiv:
%
% Authors: Georg Stadler and Chen Li, Courant Institute, NYU
% Date: 3/21/18

% clear all;

% Parameters
n = 128;            % mesh size
itermax = 55;      % iteration maximum
operator = 'helmholtz'; % operator = 'laplace';
alpha = 5e-5;      % regularization params
beta =  5e-4;
epsilon = 1e-7;
rewnew_iter = 15;  % no of initial norm reweighting iters
r = 150;           % no of vectors in low rank approximation of A^-* A^-1
r_tilde = 16;      % r_tilde
cg_iter = 3;       % no of cg iteratins per Newton step
overrel_par = 1.5; % overrelaxation parameter

h = 1/(n+1);
[xgrid,ygrid] = meshgrid(h:h:1-h,h:h:1-h);
switch operator
    case 'laplace'    % data Laplace example
	ydd = sin(2*pi*xgrid).*sin(2*pi*ygrid).*exp(2*xgrid)/6;
	ff = 0 + 0*cos(4*pi*xgrid).*cos(2*pi*ygrid);
    case 'helmholtz'  % data Helmholtz example
	ydd = 0*xgrid;
	ff = 0*xgrid;
end
f = ff(:); yd = ydd(:);

% building finite difference matrices
e = ones(n,1);
E = spdiags([-e 4*e -e], -1:1, n, n);
F = spdiags([e zeros(n,1) e], -1:1, n, n);   % -Laplace in 1D
% -Laplace on unit square with Neumann BC on lower edge
A = (kron(eye(n),E) + kron(F,-eye(n)));
A(1:n,1:n) = A(1:n,1:n) - eye(n);
switch operator
    case 'laplace'      % Laplace operator
	A = 1/h/h*A;
    case 'helmholtz' 	% Helmholtz operator
	kappa = 12;
	A = 1/h/h*A - kappa^2*speye(n^2);
    otherwise
	error('selected operator does not exist');
end
Nbc = sparse(n^2,1);

% precission operator for random bdry condition; Cinv = Mass * A
Cinv = .25/h*spdiags([-e 2*e -e], -1:1, n, n);
Cinv12 = chol(Cinv);
C12 = inv(Cinv12);
% random draw from N(0, invC)
rng(111);
sample = randn(n,1);
m = C12 * sample;
Nbc(1:n) = m / h;

y = A \ (f - Nbc);
figure(1); title('uncontrolled state');
mesh(xgrid,ygrid,reshape(y,n,n));

% low ranking of inv(A*A)
switch operator
    case 'laplace'
	[Achol,p,P] = chol(A);
	[U2, D2] = eigs(@(x)AAsolveSPD(x,Achol,P),n*n,r);
    case 'helmholtz'
	[L,D,P] = ldl(A);
	[U2, D2] = eigs(@(x)AAsolve(x,L,P,D),n*n,r);
    otherwise
	error()
end

% low ranking of Er*Fr' = inv(A*A*Cinv12)
[U1,D1,Fr_t] = svds(U2*(D2*(U2'*[C12 / h; zeros(n*n-n,n)])), r_tilde);
Er_t = U1*D1;

% iteration initialization
nu = ones(n*n,1);
optcon = 0; 
norm_xaxis = []; norm_grad = [];
w_0 = A \ (yd - (A \ f));
fprintf('Iter         |J_grad|           |nu_old - nu|\n');

for iter = 1:itermax
    D = alpha * speye(n*n) + spdiags(beta.*nu,0,n*n,n*n);
    Dinv = inv(D);

    % Sherman-Morisson-Woodbury for inv(D + A*A)
    F = inv(D2) + U2'*Dinv*U2;
    invF = inv(F);

    nu_old = nu;

    % compute gradient and define Hessian operator
    [u0, SnuEr_t, G, re_nu] = J_grad(n, invF, U2, Dinv, w_0, Er_t, epsilon, nu_old);
    H = @(z) J_Hess(z, invF, U2, Dinv, u0, SnuEr_t, nu_old, beta);
    fprintf('%3d:   %16.12f ', iter, norm(G)*h)

    if iter <= rewnew_iter   % use over-relaxed reweighting
        if iter == 1
            nu = re_nu;
        else
            nu = (1 - overrel_par)*nu + overrel_par*re_nu;
        end
        norm_xaxis = [norm_xaxis, iter];
        norm_grad = [norm_grad, norm(G)*h];
        fprintf('     %16.12f', norm(nu - nu_old)/norm(nu_old))
    else                     % use Newton-CG for reweighted problem
        % preconditioner
        % Prod_F_U = invF * U2';
        Prematrix = PrematrixSetup(nu_old, invF, beta, U2, Dinv, u0, SnuEr_t);
        prec = @(x)Prematrix.*x;

        [delta_nu,flag,relres,~,resvec] = pcg(H, -G, 1e-10, cg_iter, prec, []);
        fprintf('     %16.12f', norm(delta_nu)/norm(nu_old))
        nu = nu_old + delta_nu;
        % norm_xaxis = [norm_xaxis, norm_xaxis(end) + 2*(r + r_tilde + cg_iter*r_tilde)/(r + 2*r_tilde)];
        norm_xaxis = [norm_xaxis, iter];
        norm_grad = [norm_grad, norm(G)*h];
    end

    % plotting
    figure(2); subplot(2,2,1);
    surf(xgrid,ygrid,ydd);
    title('desired state');

    optcon_old = optcon;
    optcon = u0 + SnuEr_t * Fr_t' * sample;

    subplot(2,2,2);
    surf(xgrid, ygrid, reshape(optcon,n,n));
    title('optimal control');

    subplot(2,2,3);
    nnu = reshape(nu,n,n);
    surf(xgrid, ygrid, 1./nnu);
    title('1/nu');

    state = A \ (f - Nbc + optcon);
    subplot(2,2,4);
    surf(xgrid,ygrid,reshape(state,n,n));
    title('controlled state')
    fprintf('\n');
    %    fprintf('|u^k-u^{k+1}|_L2 = %.12f \n', norm(optcon-optcon_old,'fro')*h);
    pause(.05);
end

% numerical estimation of truncation error for different r

D = alpha * speye(n*n) + spdiags(beta.*nu,0,n*n,n*n);
Dinv = inv(D);
F = inv(D2) + U2'*Dinv*U2; % Sherman-Morisson-Woodbury for inv(D + A*A)
invF = inv(F);
nucl_norm = sum(eigs(invF));
fprintf(' %16.12f\n', nucl_norm)


% plot of the convergence rate
figure(3);
semilogy(norm_xaxis, norm_grad);
% print -depsc norm_grad


% save the plot of convergence rate
%{
xnorm = [norm_xaxis; norm_grad];
fileID = fopen('../data_1/Fig_4/plot_4/n_32.txt','w');
fprintf(fileID,'%6s %12s\n','x','norm_grad');
fprintf(fileID,'%6.2f %12.15f\n', xnorm);
fclose(fileID);
%}


return;


nnu = reshape(nu,n,n);
figure(9);
surf(xgrid(1:end,1:end),ygrid(1:end,1:end),1./nnu(1:end,1:end));
hold on;
%contour(xgrid(1:end,1:end),ygrid(1:end,1:end),1./nnu(1:end,1:end),[1e-5,1e-5],'Linewidth',1,'Linecolor','black');
axis equal;
view(-90,90);
colormap(1-gray)
shading interp
axis off
hold off
print -depsc ex2_nu


% Plot of different trajectories of stochastic process
rng(112);
sample1 = randn(n,1);
rng(1);
sample2 = randn(n,1);
rng(111112);
sample3 = randn(n,1);

sample = sample1;
optcon = u0 + E * sample;
uu = reshape(optcon,n,n);
figure(10);
mesh(xgrid(1:2:end,1:2:end),ygrid(1:2:end,1:2:end),uu(1:2:end,1:2:end));
title('optimal control u');
colormap([0 0 0]);
zrange = 1.1*max(-min(min(uu)),max(max(uu)));
axis([0,1,0,1,-zrange,zrange])
print -depsc ex2_u_s1
figure(11);
plot([0,h:h:1-h,1],[0;C12*sample;0],'Linewidth', 2);
print -depsc ex2_u_m1


xxx=[0,h:h:1-h,1]; plot(xxx,[0;C12*sample1;0],'b-',xxx,[0;C12*sample2;0],'g-.', xxx,[0;C12*sample3;0],'r:', 'Linewidth', 2);
legend('m_1','m_2','m_3');
print -depsc ex2_m123


return;

