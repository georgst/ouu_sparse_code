function x = KKsolveSPD(b,Kchol,P)

y = Kchol \ (Kchol' \ (P'*b));

x = Kchol \ (Kchol' \ y);

x = P*x;