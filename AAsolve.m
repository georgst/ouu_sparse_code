function x = KKsolve(b,Kchol,P,D)

y = Kchol' \ (D \ (Kchol \ (P'*b)));

x = Kchol' \ (D \ (Kchol \ y));

x = P*x;